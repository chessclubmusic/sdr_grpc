use hello::say_server::{Say, SayServer};
use hello::{SayRequest, SayResponse};
use tokio::sync::mpsc;
use tonic::{transport::Server, Request, Response, Status};

mod hello;

// defining a struct for our service
#[derive(Default)]
pub struct MySay {}

// implementing rpc for service defined in .proto
#[tonic::async_trait]
impl Say for MySay {
    // specify the output of rpc call
    type SendStreamStream = mpsc::Receiver<Result<SayResponse, Status>>;

    // implement RPC stream call
    async fn send_stream(
        &self,
        request: Request<SayRequest>,
    ) -> Result<Response<Self::SendStreamStream>, Status> {
        // create a queue/channel
        let (mut tx, rx) = mpsc::channel(4);
        // create a new task
        tokio::spawn(async move {
            for _ in 0..4 {
                // send response to our channel
                tx.send(Ok(SayResponse {
                    message: format!("hello {}", request.get_ref().name),
                }))
                .await
                .unwrap();
            }
        });
        Ok(Response::new(rx))
    }
    //out rpc implemented as function
    async fn send(&self, request: Request<SayRequest>) -> Result<Response<SayResponse>, Status> {
        // return response as SayResponse message as defined in .proto
        Ok(Response::new(SayResponse {
            // reading data from request which is a wrapper around our SayRequest message
            message: format!("hello {}", request.get_ref().name),
        }))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // define address for our service
    let addr = "[::1]:50051".parse().unwrap();

    // create a service
    let say = MySay::default();
    println!("Server listening on {}", addr);

    // add our service to our server
    Server::builder()
        .add_service(SayServer::new(say))
        .serve(addr)
        .await?;

    Ok(())
}
