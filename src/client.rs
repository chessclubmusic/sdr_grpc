use hello::say_client::SayClient;
use hello::SayRequest;

mod hello;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    //create a channel (connection) to server
    let channel = tonic::transport::Channel::from_static("http://[::1]:50051")
        .connect()
        .await?;
    // create gRPC client from channel
    let mut client = SayClient::new(channel);
    // create a new request
    let request = tonic::Request::new(SayRequest {
        name: String::from("alexander"),
    });
    // send request and wait for response
    let mut response = client.send_stream(request).await?.into_inner();
    //let response = client.send(request).await?.into_inner();
    //println!("RESPONSE={:?}", response);
    while let Some(res) = response.message().await? {
        println!("Note = {:?}", res);
    }
    Ok(())
}
